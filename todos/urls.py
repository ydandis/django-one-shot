from django.urls import path
from todos.views import (
    list_view,
    list_details,
    create_list,
    update_details,
    delete_item,
    create_item,
    update_item,
)

urlpatterns = [
    path("todos/", list_view, name="todo_list_list"),
    path("todos/<int:id>", list_details, name="todo_list_detail"),
    path("todos/create/", create_list, name="create"),
    path("todos/<int:id>/edit/", update_details, name="edit_details"),
    path("todos/<int:id>/delete/", delete_item, name="todo_list_delete"),
    path("todos/items/create/", create_item, name="todo_item_create"),
    path("todos/items/<int:id>/edit/", update_item, name="todo_item_update"),
]
