from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def list_view(request):
    todolist = TodoList.objects.all
    count = TodoItem.objects.all().count()
    context = {
        "todolist_object": todolist,
        "count": count,
    }
    return render(request, "todos/todos.html", context)


def list_details(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "details": details,
    }

    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_details(request, id):
    details = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=details)
        if form.is_valid():
            details = form.save()
            return redirect("todo_list_detail", id=details.id)
    else:
        form = TodoListForm(instance=details)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def delete_item(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create1.html", context)


def update_item(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


# Create your views here
